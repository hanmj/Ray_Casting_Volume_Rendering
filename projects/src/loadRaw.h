#pragma once
#include <iostream>
#include <fstream>
#include "glad/glad.h"
#include "GLFW/glfw3.h"

/*
* LOAD RAW DATA
*/


struct Data{
    char* filename;
    std::string dataType;
    GLenum GL_dataType;
    void* raw_data_ptr = NULL;
    unsigned short* dat_data_ptr;
    int dimX;
    int dimY;
    int dimZ;
    int depth;
};

void loadRaw(Data &data);
void loadDat(Data &data);
