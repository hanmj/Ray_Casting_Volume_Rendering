#include "glad/glad.h"
#include "GLFW/glfw3.h"
#include "loadShaders.h"
#include "processVolume.h"
#include <iostream>
#ifdef USE_GLM
# include <glm/glm.hpp>
# include <glm/ext.hpp>
# include <glm/gtc/matrix_transform.hpp>
# include <glm/gtc/type_ptr.hpp>
#else
# error "GLM is required here"
#endif


class Quad{
private:
    GLuint program = 0;
    GLuint Qvertex_array;
    GLuint Qvertex_buffer;
    GLuint posShaderLocation;
    GLuint textureShaderLocation;
    float quad_data[12] = { 1.0f, 1.0f, 0.0f,
                            -1.0f, 1.0f, 0.0f,
                            1.0f, -1.0f, 0.0f,
                            -1.0f, -1.0f, 0.0f
                            };
public:
    Quad() = default;
    void init();
    void draw(GLuint texture);

};

class FrontFace{
private:
    GLuint program = 0;
    GLuint Fvertex_array;
    GLuint Fvertex_buffer;
    GLuint Ftexture_buffer;
    GLuint FposShaderLocation;
    GLuint FtexShaderLocation;
    GLuint mvpShaderLocation;
public:
    FrontFace() = default;
    void init();
    void bind();
    void draw(glm::mat4 mvp, Vertices vertices);
};

class BackFace{
private:
    GLuint program = 0;
    GLuint Fvertex_array;
    GLuint Fvertex_buffer;
    GLuint Ftexture_buffer;
    GLuint FposShaderLocation;
    GLuint FtexShaderLocation;
    GLuint FtextureShaderLocation;
    GLuint mvpShaderLocation;
public:
    BackFace() = default;
    void init();
    void bind();
    void draw(glm::mat4 mvp, Vertices vertices, GLuint frontface_texture);
};

class volumeObj{
private:
    GLuint program = 0;
    GLuint vertex_array;
    GLuint vertex_buffer;
    GLuint texture_buffer;
    // variables for shaders
    GLuint posShader;
    GLuint texShader;
    GLuint mvpShader;
    GLuint dataTexShader;
    GLuint dirTexShader;
    GLuint tfTexShader;
    GLuint sample_rateShader;
    GLuint sample_stepShader;
    GLuint M_Shader;

public:
    void init();
    void bind();
    void active(glm::mat4 mvp, GLuint texture3D, GLuint textureTF, GLuint textureDIR, float sample_rate, float sample_step, int M);
    void draw(Vertices vertices);
};
