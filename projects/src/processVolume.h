#pragma once
//#include "camera.h"
#include "control.h"
#include "glad/glad.h"
#include "GLFW/glfw3.h"
#ifdef USE_GLM
# include <glm/fwd.hpp>
# include <glm/glm.hpp>
# include <glm/ext.hpp>
#else
# error "GLM is required here"
#endif

#include <iostream>
#include <vector>
#include <algorithm>


struct Vertices{
    std::vector<GLfloat> vertex_array;
    std::vector<GLfloat> texture_array;
};

Vertices constructFacetoVertices(std::vector<glm::vec3> volume, std::vector<glm::vec3> texture);
