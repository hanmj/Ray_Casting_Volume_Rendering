#include "viewPort.h"
#include "widgets/TransferFunctionWidget.h"
# include <glm/ext.hpp>

extern Data volumeData;
// global window

GLFWwindow* window;
Camera camera;

std::vector<glm::vec4> projTransVolume;
std::vector<glm::vec4> mvTransVolume;

GLuint texture3D; // volume
GLuint textureTF; // transfer function
GLuint tex_tfn_opaque;

static framebufferObj fbo;
static framebufferObj frontfaceFBO;
static framebufferObj backfaceFBO;
static FrontFace frontface;
static BackFace backface;
static Quad quad; // used to render screen to quad texture
static volumeObj volume;

extern float sample_rate;
float sample_step = 0.001f;
extern int M ;
float plane_sample_rate;
float unit_voxel;
static Vertices vertices;

std::vector<glm::vec3> volume_bb {
  glm::vec3(-1.0f, 1.0f, 1.0f),
  glm::vec3(-1.0f, -1.0f, 1.0f),
  glm::vec3(1.0f, -1.0f, 1.0f),

  glm::vec3(1.0f, 1.0f, 1.0f),
  glm::vec3(-1.0f, 1.0f, 1.0f),
  glm::vec3(1.0f,-1.0f, 1.0f),

  glm::vec3(1.0f,-1.0f,-1.0f),
  glm::vec3(1.0f, 1.0f, 1.0f), 
  glm::vec3(1.0f, -1.0f, 1.0f),

  glm::vec3(1.0f, 1.0f, 1.0f),
  glm::vec3(1.0f,-1.0f, -1.0f),
  glm::vec3(1.0f, 1.0f, -1.0f),

  glm::vec3(1.0f, 1.0f, 1.0f),
  glm::vec3(1.0f, 1.0f,-1.0f),
  glm::vec3(-1.0f, 1.0f,-1.0f),

  glm::vec3(1.0f, 1.0f, 1.0f),
  glm::vec3(-1.0f, 1.0f,-1.0f),
  glm::vec3(-1.0f, 1.0f, 1.0f),

     glm::vec3(-1.0f,-1.0f,-1.0f),
    glm::vec3(-1.0f,-1.0f, 1.0f),
    glm::vec3(-1.0f, 1.0f, 1.0f),

    glm::vec3(1.0f, 1.0f,-1.0f),
    glm::vec3(-1.0f,-1.0f,-1.0f),
    glm::vec3(-1.0f, 1.0f,-1.0f),

    glm::vec3(1.0f,-1.0f, 1.0f),
    glm::vec3(-1.0f,-1.0f,-1.0f),
    glm::vec3(1.0f,-1.0f,-1.0f),

    glm::vec3(1.0f, 1.0f,-1.0f),
    glm::vec3(1.0f,-1.0f,-1.0f),
    glm::vec3(-1.0f,-1.0f,-1.0f),

    glm::vec3(-1.0f,-1.0f,-1.0f),
    glm::vec3(-1.0f, 1.0f, 1.0f),
    glm::vec3(-1.0f, 1.0f,-1.0f),
    
    glm::vec3(1.0f,-1.0f, 1.0f),
    glm::vec3(-1.0f,-1.0f, 1.0f),
    glm::vec3(-1.0f,-1.0f,-1.0f)

};

  std::vector<glm::vec3> texture_bb{
    glm::vec3(0.0f, 1.0f, 1.0f),
    glm::vec3(0.0f, 0.0f, 1.0f),
    glm::vec3(1.0f, 0.0f, 1.0f),

    glm::vec3(1.0f, 1.0f, 1.0f),
    glm::vec3(0.0f, 1.0f, 1.0f),
    glm::vec3(1.0f, 0.0f, 1.0f),
   
    glm::vec3(1.0f, 0.0f, 0.0f),
    glm::vec3(1.0f, 1.0f, 1.0f),
    glm::vec3(1.0f, 0.0f, 1.0f),

    glm::vec3(1.0f, 1.0f, 1.0f),
    glm::vec3(1.0f, 0.0f, 0.0f),
    glm::vec3(1.0f, 1.0f, 0.0f),

    glm::vec3(1.0f, 1.0f, 1.0f),
    glm::vec3(1.0f, 1.0f,0.0f),
    glm::vec3(0.0f, 1.0f,0.0f),

    glm::vec3(1.0f, 1.0f, 1.0f),
    glm::vec3(0.0f, 1.0f,0.0f),
    glm::vec3(0.0f, 1.0f, 1.0f),
    glm::vec3(0.0f, 0.0f, 0.0f),
    glm::vec3(0.0f, 0.0f, 1.0f),
    glm::vec3(0.0f, 1.0f, 1.0f),

    glm::vec3(1.0f, 1.0f, 0.0f),
    glm::vec3(0.0f, 0.0f, 0.0f),
    glm::vec3(0.0f, 1.0f, 0.0f),

    glm::vec3(1.0f, 0.0f, 1.0f),
    glm::vec3(0.0f, 0.0f, 0.0f),
    glm::vec3(1.0f, 0.0f, 0.0f),

    glm::vec3(1.0f, 1.0f, 0.0f),
    glm::vec3(1.0f, 0.0f, 0.0f),
    glm::vec3(0.0f, 0.0f, 0.0f),

    glm::vec3(0.0f,0.0f, 0.0f),
    glm::vec3(0.0f, 1.0f, 1.0f),
    glm::vec3(0.0f, 1.0f, 0.0f),

    glm::vec3(1.0f, 0.0f, 1.0f),
    glm::vec3(0.0f, 0.0f, 1.0f),
    glm::vec3(0.0f, 0.0f, 0.0f)
  };
  

int ShowViewPort(){
  std::cout << "  ========== Start Show View Port ==========" << std::endl;
    /*
    ** Initialize GLFW
    */
    glfwSetErrorCallback(&glfwError);
    if (!glfwInit()) {
          fprintf( stderr, "Failed to initialize GLFW\n" );
		      return -1;
        }else{
          std::cout << std::endl;
          std::cout << "  Done Initialize GLFW  " << std::endl;
      }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, true);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, 1);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    std::cout << "  Done Initialize GLFW  " << std::endl;
    window = glfwCreateWindow(camera.getWidth(), camera.getHeight(), "Texture Based Volume Renderer", NULL, NULL);

    if (!window) { glfwTerminate();return -1; }
    std::cout << "  Done create window GLFW  " << std::endl;
    // Ensure we can capture the escape key being pressed below

  	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

    glfwSetKeyCallback(window, key_callback);
    glfwSetWindowSizeCallback(window, window_size_callback);
    glfwSetCursorPosCallback(window, cursor_position_callback);
    glfwSetMouseButtonCallback(window, ImGui_ImplGlfwGL3_MouseButtonCallback);
    glfwSetScrollCallback(window, scroll_callback);

    glfwMakeContextCurrent(window);
    gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glEnable(GL_DEPTH_TEST);
    ImGui_ImplGlfwGL3_Init(window, false);

    GLenum error = glGetError();
    std::cout << std::endl;
    std::cout << "  ========== OpenGL Info ========== " << std::endl;
    std::cout << std::endl;
    // print out some info about the graphics drivers
    std::cout << "  OpenGL version: " << glGetString(GL_VERSION) << std::endl;
    std::cout << "  GLSL version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;

	if (error != GL_NO_ERROR){
        std::cout << "  OpenGL Error: " << error << std::endl;
	}
    //Accept fragment if it closer to the camera than the former one
    //glDepthFunc(GL_LESS);

    std::cout << std::endl;
    std::cout << "  ========== Start Loading Textures ========== " << std::endl;

    /*
    ** Load Volume as 3D Texture
    */
    texture3D = loadTexture(volumeData);
    std::cout << std::endl;
    std::cout << "  == 3D Texture Location is: " << texture3D << std::endl;

    /*
    ** Load Transfer Function as 1D Texture
    */
    std::vector<GLubyte> texels;

    // auto tfnWidget =  new tfn::tfn_widget::TransferFunctionWidget
    // ([]() { return 256; },
    //  [&](const std::vector<float>& c, const std::vector<float>& a) 
    //  {
    //    texels.clear();
    //    for (int i = 0; i < 256; ++i) {
    //      texels.push_back( 255 * c[3 * i + 0]);
	  //      texels.push_back( 255 * c[3 * i + 1]);
	  //      texels.push_back( 255 * c[3 * i + 2]);
	  //      texels.push_back( 255 * a[i]);       	
    //    }

    //  });    
    std::cout << std::endl;
    loadTF(textureTF, volumeData, texels);
    loadTF(tex_tfn_opaque, volumeData, texels);
    
    std::cout << "  == Transfer Function Texture Location is: " << textureTF << std::endl;

    /*
    ** Initialize frambuffer
    */
    fbo.init(camera.getWidth(), camera.getHeight(), volumeData);
    frontfaceFBO.init(camera.getWidth(), camera.getHeight(), volumeData);
    backfaceFBO.init(camera.getWidth(), camera.getHeight(), volumeData);

    /*
     ** Initialize face object
     */
    backface.init();
    frontface.init();

    // construct face to vertices
    Vertices vertices;
    vertices = constructFacetoVertices(volume_bb, texture_bb);

    /*
    ** Initialize volume compose
    */
    volume.init();

    /*
    ** Initialize Quad Object to render screen in a 2D texture
    */
    quad.init();

    std::cout << std::endl;
    std::cout << "  ========= Done Initialize Start Rendering ==========  " << std::endl;
    /*
    ** Begin Render
    */
    //M = volumeData.depth * 3;

    while (!glfwWindowShouldClose(window))
    {
        // get mvp 
        glm::mat4 mvp = camera.getMVPMatrix();
        // render front face to texture
        frontfaceFBO.bind(0, camera.getWidth(), camera.getHeight());
        frontface.bind();
        frontface.draw(mvp, vertices);
        frontfaceFBO.unbind();
        // render back face to texture
        backfaceFBO.bind(0, camera.getWidth(), camera.getHeight());
        backface.bind();
        backface.draw(mvp, vertices, frontfaceFBO.getColorTexture(0));
        backfaceFBO.unbind();
        // render volume
        fbo.bind(0, camera.getWidth(), camera.getHeight());
        volume.bind();
        volume.active(mvp, texture3D, textureTF, backfaceFBO.getColorTexture(0), sample_rate, sample_step, M);
        volume.draw(vertices);
        RenderGUI(textureTF);
        fbo.unbind();


        quad.draw(fbo.getColorTexture(0));
        glfwSwapBuffers(window);
	// tfnWidget -> render();
        glfwPollEvents();
    } // end while
    ImGui_ImplGlfwGL3_Shutdown();
    glfwDestroyWindow(window);
    glfwTerminate();
}
