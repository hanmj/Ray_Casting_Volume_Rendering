
#include "loadRaw.h"
#include <vector>

void loadRaw(Data &data){
    std::cout << "  ========== Start Loading Raw Data ==========  " << std::endl;
    std::cout << "  Data File is [Raw]: " << data.filename << std::endl;

    std::ifstream fp(data.filename, std::ios::binary);
    //FILE *fp = fopen(data.filename.c_str(), "rb");
    data.depth = std::max(data.dimX, std::max(data.dimY, data.dimZ));
    int u_size;
    if(data.dataType == "uint8"){
        u_size = sizeof(uint8_t);
        std::cout << "  Data Type is: " << data.dataType << std::endl;
        data.GL_dataType = GL_UNSIGNED_BYTE; //uint8
    }else if(data.dataType == "uint16"){
      u_size = sizeof(uint16_t);
      std::cout << "  Data Type is: " << data.dataType << std::endl;
      data.GL_dataType = GL_UNSIGNED_SHORT; //uint16
    }

    int uCount = data.dimX * data.dimY * data.dimZ;

    std::cout << "  Data unit size is: " << u_size << std::endl;

    data.raw_data_ptr = new char[uCount * u_size];
    fp.read((char*)data.raw_data_ptr, uCount * u_size);
    fp.close();
    //fread((char*)pData, uCount, sizeof(uint8_t), fp);
    //fclose(fp);

    std::cout << "  Data Size is [Raw]: " << data.dimX << " " << data.dimY << " " << data.dimZ << std::endl;

    std::cout << "  ========== Done Parsing Raw Data ==========  " << std::endl;

}

void loadDat(Data &data){
  std::cout << "  ========== Start Loading Dat Data ==========  " << std::endl;
  std::cout << "  Data File is [Dat]: " << data.filename << std::endl;
  data.depth = std::max(data.dimX, std::max(data.dimY, data.dimZ));
  int u_size;
  if(data.dataType == "uint8"){
      u_size = sizeof(uint8_t);
      std::cout << "  Data Type is: " << data.dataType << std::endl;
      data.GL_dataType = GL_UNSIGNED_BYTE; //uint8
  }else if(data.dataType == "uint16"){
    u_size = sizeof(uint16_t);
    std::cout << "  Data Type is: " << data.dataType << std::endl;
    data.GL_dataType = GL_UNSIGNED_SHORT; //uint16
  }
  std::ifstream fp(data.filename, std::ios::binary);
  void* data_size_ptr = NULL;
  data_size_ptr = new char[3 * u_size];
  //unsigned short vuSize[3];
  fp.read((char*)data_size_ptr, 3 * u_size);
  char* size_ptr = (char*) data_size_ptr;
  int uCount = (*(size_ptr + 0)) * (*(size_ptr + 1)) * (*(size_ptr + 2));

  //int uCount = int(vuSize[0])*int(vuSize[1])*int(vuSize[2]);

  std::cout << "  Data Count is: " << uCount << std::endl;

  data.raw_data_ptr = new char[uCount * u_size];
  fp.read((char*)data.raw_data_ptr, uCount * u_size);
  fp.close();
  std::cout << "  Data Size is [Dat]: " << data.dimX << " " << data.dimY << " " << data.dimZ << std::endl;

  std::cout << "  ========== Done Parsing Dat Data ==========  " << std::endl;

}
