#pragma once
#include <iostream>
#include "glad/glad.h"
#include "GLFW/glfw3.h"
#include "trackball.h"
#include <glm/glm.hpp>
#include "glm/ext.hpp"
#include <glm/gtc/matrix_transform.hpp>

class Camera{
private:
  int width = 640;
  int height = 480;
  float aspect = (float)width / height;
  float zNear = 1.0f;
  float zFar = 50.0f;
  float fovy = 30.0f;
  glm::vec3 eye   = glm::vec3(0.f, 0.f, -8.f);
  glm::vec3 focus = glm::vec3(0.f);
  glm::vec3 up    = glm::vec3(0.f,1.f,0.f);
  glm::mat4 ViewMatrix, ProjMatrix;
  glm::mat4 mv, mvp;
  Trackball ball;

public:
  Camera(){
    ViewMatrix = glm::lookAt(eye, focus, up);
    ProjMatrix = glm::perspective(fovy/180.f*(float)M_PI,aspect, zNear, zFar);
  };

  //get camera width height
  int getWidth(){return width;}
  int getHeight(){return height;}
  float getFovy(){return fovy;}
  glm::vec3 getEye(){return eye;}
  glm::vec3 getFocus(){return focus;}
  glm::vec3 getUp(){return up;}
  glm::mat4 getProjMatrix(){return ProjMatrix;}

  glm::mat4 getMVMatrix(){
    //const glm::mat4 m = glm::rotate(glm::mat4(1.f), 0.f, glm::vec3(0,1,0));
    //mv = ViewMatrix * ball.Matrix * m;
    mv = ViewMatrix * ball.Matrix() * glm::mat4(1.0);
    return mv;
  }

  glm::mat4 getMVPMatrix(){
    mvp = ProjMatrix * getMVMatrix();
    return mvp;
  }

  //update view matrix
  void updateViewMatrix(){
    glm::vec3 dir = glm::vec3(ball.Matrix() * glm::vec4(eye - focus, 0.f));
    glm::vec3 up = glm::vec3(ball.Matrix() * glm::vec4(up, 0.0f));
    ViewMatrix = glm::lookAt(dir + focus, focus, up);
  }

  void updateProjMatrix_windowsize(size_t, size_t);

  void updateProjMatrix(float f){
    fovy = f;
    ProjMatrix = glm::perspective(fovy/180.f*(float)M_PI, aspect, zNear, zFar);
  }

  void rotate(float x, float y);
  void startRotate(float x, float y);
};
