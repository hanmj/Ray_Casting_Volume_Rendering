#include "loadTexture.h"

GLuint loadTexture(Data data){
    std::cout << std::endl;
    int maxDepth = data.depth;
    std::cout << "  ========== Load Volume As 3D Texture ========== " << std::endl;
    std::cout << "  Data Depth is: " << maxDepth << std::endl;
    char* data_ptr = (char*) data.raw_data_ptr;
    if(data_ptr == NULL){
      std::cout << "  data load Unsuccessfully " << std::endl;
    }else{
      std::cout << "  Data Load Successfully " << std::endl;
    }

    GLuint texture3D;
    glGenTextures(1, &texture3D);
    glBindTexture(GL_TEXTURE_3D, texture3D);
    std::cout << "data GL type: " << data.GL_dataType << std::endl;
    glTexImage3D(GL_TEXTURE_3D, 0, GL_RED, data.dimX, data.dimY, data.dimZ, 0, GL_RED, data.GL_dataType, data_ptr);

    //glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    std::cout << "  ========== DONE ==========" << std::endl;
    return texture3D;
}
