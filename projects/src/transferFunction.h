#include "glad/glad.h"
#include "GLFW/glfw3.h"
#include "loadRaw.h"
#ifdef USE_GLM
# include <glm/glm.hpp>
# include <glm/gtc/matrix_transform.hpp>
# include <glm/gtc/type_ptr.hpp>
#else
# error "GLM is required here"
#endif
#include <vector>

void loadTF(GLuint &textureTF, Data data,  const std::vector<GLubyte> &);
void updateTFN_custom(const GLuint textureID, const GLubyte* palette,
		      const int width, const int height);
