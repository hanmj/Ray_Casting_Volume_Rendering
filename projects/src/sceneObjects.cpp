#include "sceneObjects.h"

void Quad::init(){
    std::cout << std::endl;
    std::cout << " ========== Plane Init ========= " << std::endl;
    std::cout << " == Loading plane shaders: " << std::endl;

    program = loadShader("VQuad.glsl","FQuad.glsl");

    glGenVertexArrays(1, &Qvertex_array);
    glBindVertexArray(Qvertex_array);

    glGenBuffers(1, &Qvertex_buffer);
    glBindBuffer(GL_ARRAY_BUFFER, Qvertex_buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(quad_data), quad_data, GL_STATIC_DRAW);

    posShaderLocation = glGetAttribLocation(program, "pos");
    textureShaderLocation = glGetUniformLocation(program, "texture2D");
    std::cout << " ========== DONE ========== " << std::endl;
}

void Quad::draw(GLuint texture){
    glUseProgram(program);
    glBindVertexArray(Qvertex_array);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture);
    glUniform1i(textureShaderLocation, 0);

    glEnableVertexAttribArray(posShaderLocation);
    glBindBuffer(GL_ARRAY_BUFFER, Qvertex_buffer);
    glVertexAttribPointer(posShaderLocation, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glDisableVertexAttribArray(posShaderLocation);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    glUseProgram(0);
}

void FrontFace::init(){
    std::cout << std::endl;
    std::cout << " ========== Face Init ========= " << std::endl;
    std::cout << " == Loading face shaders: " << std::endl;

    program = loadShader("VFrontFace.glsl","FFrontFace.glsl");

    glGenVertexArrays(1, &Fvertex_array);
    glBindVertexArray(Fvertex_array);

    glGenBuffers(1, &Fvertex_buffer);
    glGenBuffers(1, &Ftexture_buffer);

    FposShaderLocation = glGetAttribLocation(program, "pos");
    FtexShaderLocation = glGetAttribLocation(program, "tex");
    mvpShaderLocation = glGetUniformLocation(program, "mvp");

    std::cout << " ========== DONE ========== " << std::endl;

}

void FrontFace::bind(){
    glUseProgram(program);
    glBindVertexArray(Fvertex_array);
}

void FrontFace::draw(glm::mat4 mvp, Vertices vertices){
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    glUniformMatrix4fv(mvpShaderLocation, 1, GL_FALSE, &mvp[0][0]);
    size_t data_size = vertices.vertex_array.size();

    glEnableVertexAttribArray(FposShaderLocation);
    glBindBuffer(GL_ARRAY_BUFFER, Fvertex_buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * data_size, vertices.vertex_array.data(), GL_STATIC_DRAW);
    glVertexAttribPointer(FposShaderLocation, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);

    glEnableVertexAttribArray(FtexShaderLocation);
    glBindBuffer(GL_ARRAY_BUFFER, Ftexture_buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * data_size, vertices.texture_array.data(), GL_STATIC_DRAW);
    glVertexAttribPointer(FtexShaderLocation, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glDrawArrays(GL_TRIANGLES, 0, data_size / 3);
    glDisable(GL_CULL_FACE);
}

void BackFace::init(){
    std::cout << std::endl;
    std::cout << " ========== BackFace Init ========= " << std::endl;
    std::cout << " == Loading face shaders: " << std::endl;

    program = loadShader("VBackFace.glsl","FBackFace.glsl");

    glGenVertexArrays(1, &Fvertex_array);
    glBindVertexArray(Fvertex_array);

    glGenBuffers(1, &Fvertex_buffer);
    glGenBuffers(1, &Ftexture_buffer);

    FposShaderLocation = glGetAttribLocation(program, "pos");
    FtexShaderLocation = glGetAttribLocation(program, "tex");
    mvpShaderLocation = glGetUniformLocation(program, "mvp");
    FtextureShaderLocation = glGetAttribLocation(program, "frontface_texture");

    std::cout << " ========== DONE ========== " << std::endl;

}

void BackFace::bind(){
    glUseProgram(program);
    glBindVertexArray(Fvertex_array);
}

void BackFace::draw(glm::mat4 mvp, Vertices vertices, GLuint frontface_texture){
    glEnable(GL_CULL_FACE);
    glCullFace(GL_FRONT);
    glUniformMatrix4fv(mvpShaderLocation, 1, GL_FALSE, &mvp[0][0]);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, frontface_texture);
    glUniform1i(FtextureShaderLocation, 0);

    size_t data_size = vertices.vertex_array.size();

    glEnableVertexAttribArray(FposShaderLocation);
    glBindBuffer(GL_ARRAY_BUFFER, Fvertex_buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * data_size, vertices.vertex_array.data(), GL_STATIC_DRAW);
    glVertexAttribPointer(FposShaderLocation, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);

    glEnableVertexAttribArray(FtexShaderLocation);
    glBindBuffer(GL_ARRAY_BUFFER, Ftexture_buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * data_size, vertices.texture_array.data(), GL_STATIC_DRAW);
    glVertexAttribPointer(FtexShaderLocation, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glDrawArrays(GL_TRIANGLES, 0, data_size / 3);
    glDisable(GL_CULL_FACE);
}

void volumeObj::init(){
    std::cout << std::endl;
    std::cout << " ========== Volume Init ========= " << std::endl;
    std::cout << " == Loading volume shaders: " << std::endl;

    program = loadShader("VRayCastingVolumeRenderer.glsl","FRayCastingVolumeRenderer.glsl");

    glGenVertexArrays(1, &vertex_array);
    glBindVertexArray(vertex_array);

    glGenBuffers(1, &vertex_buffer);
    glGenBuffers(1, &texture_buffer);

    posShader = glGetAttribLocation(program, "pos");
    texShader = glGetAttribLocation(program, "tex");

    mvpShader = glGetUniformLocation(program, "mvp");
    dirTexShader = glGetUniformLocation(program, "dirTex");
    dataTexShader = glGetUniformLocation(program, "dataTex");
    tfTexShader = glGetUniformLocation(program, "tfTex");
    // sample_rateShader = glGetUniformLocation(program, "sample_rate");
    // WARN(sample_rateShader != -1, "Fail to find 'sample_rateShader' location"); 
    sample_stepShader = glGetUniformLocation(program, "sample_step");
    M_Shader = glGetUniformLocation(program, "M");

    std::cout << " ========== DONE ========== " << std::endl;
}

void volumeObj::bind(){
    glUseProgram(program);
    glBindVertexArray(vertex_array);
}

void volumeObj::active(glm::mat4 mvp, GLuint texture3D, GLuint textureTF, GLuint textureDIR, float sample_rate, float sample_step, int M){

    // glUniform1f(sample_rateShader, sample_rate);
    glUniform1f(sample_stepShader, sample_step);
    glUniform1i(M_Shader, M);
    glUniformMatrix4fv(mvpShader, 1, GL_FALSE, &mvp[0][0]);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureDIR);
    glUniform1i(dirTexShader, 0);

  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_3D, texture3D);
  glUniform1i(dataTexShader, 1);

  glActiveTexture(GL_TEXTURE2);
  glBindTexture(GL_TEXTURE_2D, textureTF);
  glUniform1i(tfTexShader, 2);
}

void volumeObj::draw(Vertices vertices){
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);

    size_t data_size = vertices.vertex_array.size();

    glEnableVertexAttribArray(posShader);
    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * data_size, vertices.vertex_array.data(), GL_STATIC_DRAW);
    glVertexAttribPointer(posShader, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);

    glEnableVertexAttribArray(texShader);
    glBindBuffer(GL_ARRAY_BUFFER, texture_buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * data_size, vertices.texture_array.data(), GL_STATIC_DRAW);
    glVertexAttribPointer(texShader, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glDrawArrays(GL_TRIANGLES, 0, data_size / 3);
    glDisable(GL_CULL_FACE);
}
