#include "transferFunction.h"



void loadTF(GLuint &textureTF, Data data,  const std::vector<GLubyte> &texels){

    glGenTextures(1, &textureTF);
    glBindTexture(GL_TEXTURE_2D, textureTF);
    //glTexImage2D(GL_TEXTURE_1D, 0, GL_RGBA, ColorKnots.size(), 1, 0, GL_RGBA, GL_UNSIGNED_BYTE, ColorKnots);
    //glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texels.size() / 4, 1, 0, GL_RGBA, data.GL_dataType, texels.data());
    // ... nice trilinear filtering ...
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
}

void updateTFN_custom(const GLuint textureID, const GLubyte* palette,
		      const int width, const int height)
{
  glBindTexture(GL_TEXTURE_2D, textureID);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA,
	       GL_UNSIGNED_BYTE, static_cast<const void*>(palette));
}
