#version 410 core

layout(location = 0) out vec4 fragColor;

uniform sampler2D  dirTex;
uniform sampler3D  dataTex;
uniform sampler2D  tfTex;

//uniform float sample_rate;
uniform  float sample_step;
uniform int M;

in vec3 data_texCoords;;
in vec2 backface_texCoords;

void main(){
    // access dir and l from DIR texture
    vec4 DIR = texture(dirTex, backface_texCoords);
    float l_temp = DIR.w;
    float l = l_temp;
    vec3 dir_temp = DIR.xyz;
    vec3 dir = (dir_temp - 0.5) / 0.5 ;

    if( l == 0.0){
        discard;
    }
    vec4 colorAcum = vec4(0.0);
    vec3 voxelCoord = data_texCoords;
    vec3 deltaDir = dir * sample_step;
    float deltaDirLen = length(deltaDir);
    vec4 bgColor = vec4(0.0, 0.0, 0.0, 0.0);
    float lengthAcum = 0.0;
    float intensity = 0.0;
    vec4  colorSample;

    for (int i = 0; i < M; ++i){
        intensity = texture(dataTex, voxelCoord).r;
        colorSample = texture(tfTex, vec2(intensity, 0.0));
        if(colorSample.a > 0.0){
            colorSample.a = 1.0 - pow(1.0 - colorSample.a, sample_step * 200.0f );
            colorAcum.rgb += (1.0 - colorAcum.a) * colorSample.rgb *colorSample.a;
            colorAcum.a += (1.0 - colorAcum.a) * colorSample.a;
        }
        voxelCoord += deltaDir;
        lengthAcum += deltaDirLen;

        if(lengthAcum >= l){
            colorAcum.rgb = colorAcum.rgb * colorAcum.a + (1 - colorAcum.a) * bgColor.rgb;
           break;
       }else if(colorAcum.a > 1.0){
            colorAcum.a = 1.0;
            break;
        }
    }
    fragColor = colorAcum;
}
