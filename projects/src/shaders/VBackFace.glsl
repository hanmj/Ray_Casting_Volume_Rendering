#version 410 core

layout(location = 0) in vec3 pos;
layout(location = 1) in vec3 tex;

uniform mat4 mvp;

out vec2 front_texCoords;
out vec3 back_texCoords;

void main(){
    gl_Position = mvp * vec4(pos, 1.0);
    vec4 temp = mvp * vec4(pos, 1.0);
    float x = temp.x / temp.w;
    float y = temp.y / temp.w;
    front_texCoords = vec2(x, y) * 0.5 + 0.5; // used to access front face texture
    back_texCoords = tex;
}
