#version 410 core

layout(location = 0) in vec3 pos;
layout(location = 1) in vec3 tex;

uniform mat4 mvp;

out vec3 data_texCoords;
out vec2 backface_texCoords;

void main(){
    gl_Position = mvp *  vec4(pos, 1.0);
    data_texCoords = tex;
    vec4 temp = mvp * vec4(pos, 1.0);
    float x = temp.x / temp.w;
    float y = temp.y / temp.w;
    backface_texCoords = vec2(x, y) * 0.5 + 0.5;
}
