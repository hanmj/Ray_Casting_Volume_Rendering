#version 410 core

layout(location = 0) out vec4 fragColor;

uniform sampler2D frontface_texture;

in vec2 front_texCoords;
in vec3 back_texCoords;


void main(){
    vec4 TMP = texture(frontface_texture, front_texCoords);
    vec4 COL = vec4(back_texCoords, 1.0);
    vec4 temp = COL - TMP;
    vec3 dir = normalize(temp.xyz);
    float l = length(temp.xyz);
    float map_l =  l; 
    vec3 map = dir * 0.5 + 0.5;
    vec3 color = vec3(map.x, map.y, map.z);

    fragColor = vec4(color, map_l);
}
