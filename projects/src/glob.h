#include "glad/glad.h"
#include "GLFW/glfw3.h"
#ifdef USE_GLM
# include <glm/fwd.hpp>
#else
# error "GLM is required here"
#endif


void RenderGUI(GLuint);bool CapturedByGUI();
