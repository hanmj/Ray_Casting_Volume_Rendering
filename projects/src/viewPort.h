#include "glad/glad.h"
#include "GLFW/glfw3.h"
#include "glob.h"
#include "control.h"
#include "trackball.h"
#include "processVolume.h"
#include "transferFunction.h"
#include "loadTexture.h"
#include "sceneObjects.h"
#include "framebuffer.h"
#include <iostream>
#include <stdio.h>
#ifdef USE_GLM
# include <glm/glm.hpp>
# include <glm/ext.hpp>
# include <glm/gtc/matrix_transform.hpp>
# include <glm/gtc/type_ptr.hpp>
#else
# error "GLM is required here"
#endif
#include "imgui.h"
#include "examples/opengl3_example/imgui_impl_glfw_gl3.h"
extern Camera camera;

int ShowViewPort();

static void glfwError(int id, const char* description)
{
  std::cout << description << std::endl;
}

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
  //if (ImGui::GetIO().WantCaptureKeyboard) { return; }

  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
  { glfwSetWindowShouldClose(window, GLFW_TRUE); }

  //ImGui_ImplGlfwGL3_KeyCallback(window, key, scancode, action, mods);
}

 static void cursor_position_callback(GLFWwindow* window, double xpos, double ypos)
 {
    //if (ImGui::GetIO().WantCaptureMouse) { return;}
    if(!CapturedByGUI()){
         int left_state  = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
   if (left_state == GLFW_PRESS) {camera.rotate(xpos, ypos); }
   else { camera.startRotate(xpos, ypos); }

    }

 }


static void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
  float fovy = camera.getFovy() - 0.2 * yoffset;
  camera.updateProjMatrix(fovy);
}

static void window_size_callback(GLFWwindow* window, int width, int height)
{
  glViewport(0, 0, width, height);
  camera.updateProjMatrix_windowsize(width, height);
}
