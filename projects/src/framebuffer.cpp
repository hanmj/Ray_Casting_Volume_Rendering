#include "framebuffer.h"

/*
** Methods Of framebufferObj Class
*/

extern framebufferObj fbo;

void framebufferObj::init(unsigned int width, unsigned int height, Data data){
	clear();
	FBO_colorbuffer = new GLuint[1]; // one to draw, one to read
	glGenFramebuffers(1, &FBO);
	glBindFramebuffer(GL_FRAMEBUFFER, FBO); // bind to pipeline

	generateDepthTexture(width, height);//generate empty texture

	std::cout << std::endl;
	//bind textures to pipeline. texture_depth is optional
	std::cout << " ========== BINGDING TEXTURE TO PIPLINE ========== " << std::endl;

	glGenTextures(1, FBO_colorbuffer);
	for(size_t i = 0; i < 1; i++){
		generateColorTexture(width, height, i, data);//generate empty texture
	}

	std::cout << " ========== Done ==========" << std::endl;
	//Check for FBO completeness
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE){
		std::cout << "Error! FrameBuffer is not complete" << std::endl;
		std::cin.get();
		std::terminate();
	}
	//unbind framebuffer
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void framebufferObj::reset(unsigned int width, unsigned int height, Data data){
	glBindFramebuffer(GL_FRAMEBUFFER, FBO);
	for (size_t i = 0; i < 2; i++) {
		generateColorTexture(width, height, i, data);//generate empty texture
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void framebufferObj::generateColorTexture(unsigned int width, unsigned int height, size_t i, Data data){
	GLubyte *Data;
	if(data.dataType == "uint8"){
		Data = new GLubyte[width * height * 4]();
	}else if(data.dataType == "uint16"){
		Data = new GLubyte[width * height * 8]();
	}
	glBindTexture(GL_TEXTURE_2D, FBO_colorbuffer[i]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, data.GL_dataType, Data);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, FBO_colorbuffer[i], 0);
	glBindTexture(GL_TEXTURE_2D, 0);
	delete [ ] Data;
}

void framebufferObj::generateDepthTexture(unsigned int width, unsigned int height){
	glGenRenderbuffers(1, &FBO_depthbuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, FBO_depthbuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
    	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, FBO_depthbuffer);
  	glBindRenderbuffer(GL_RENDERBUFFER, 0);
}

void framebufferObj::bind(size_t i, unsigned int width, unsigned int height){
    glGetIntegerv(GL_VIEWPORT, viewport);
    glViewport(0, 0, width, height);
    glBindFramebuffer(GL_FRAMEBUFFER, FBO);
    GLenum DrawBuffers[1] = { GL_COLOR_ATTACHMENT0};
    glDrawBuffers(1, DrawBuffers);
}

void framebufferObj::unbind(){
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glViewport(viewport[0], viewport[1], viewport[2], viewport[3]);
}

void framebufferObj::clear() {
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glDeleteFramebuffers(1, &FBO);
    glDeleteRenderbuffers(1, &FBO_depthbuffer);
    if (FBO_colorbuffer) {
      glDeleteTextures(1, FBO_colorbuffer);
      delete [] FBO_colorbuffer;
      FBO_colorbuffer = NULL;
    }
}
