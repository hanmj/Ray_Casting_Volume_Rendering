
// Include GLM
#include <glm/glm.hpp>

#include <glm/gtc/matrix_transform.hpp>
using namespace glm;

#include "control.h"

extern GLFWwindow* window;
extern Camera camera;

void Camera::updateProjMatrix_windowsize(size_t width, size_t height){
  aspect = width / (float) height;
  width  = width;
  height = height;
  ProjMatrix = glm::perspective(fovy/180.f*(float)M_PI,aspect, zNear, zFar);
}

void Camera::startRotate(float x, float y){
  glm::vec2 p = glm::vec2(2.f * (float)x / width - 1.f, 1.f - 2.f * (float)y / height);
  ball.BeginDrag(p.x, p.y);

}

void Camera::rotate(float x, float y){
  glm::vec2 p = glm::vec2(2.f * (float)x / width - 1.f, 1.f - 2.f * (float)y / height);
  ball.Drag(p.x, p.y);
}
