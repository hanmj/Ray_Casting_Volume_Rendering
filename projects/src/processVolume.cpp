#include "processVolume.h"
#ifdef USE_GLM
# include <glm/glm.hpp>
# include <glm/gtc/matrix_transform.hpp>
# include <glm/gtc/type_ptr.hpp>
#else
# error "GLM is required here"
#endif
#include <cmath>

extern Camera camera;

Vertices constructFacetoVertices(std::vector<glm::vec3> volume, std::vector<glm::vec3> texture){
    Vertices vertices;
    for(int i = 0; i < volume.size(); i++){
        vertices.vertex_array.push_back(volume[i].x);
        vertices.vertex_array.push_back(volume[i].y);
        vertices.vertex_array.push_back(volume[i].z);
        vertices.texture_array.push_back(texture[i].x);
        vertices.texture_array.push_back(texture[i].y);
        vertices.texture_array.push_back(texture[i].z);
    }
    return vertices;
}
