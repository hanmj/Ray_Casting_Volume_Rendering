#pragma once
#include <iostream>
#include <vector>
#include <algorithm>
#include "glad/glad.h"
#include "GLFW/glfw3.h"
#include "loadRaw.h"

class framebufferObj{

private:
  GLuint FBO = 0;
  GLuint* FBO_colorbuffer = NULL;
  GLuint FBO_depthbuffer = 0;
  GLint   viewport[4];

public:

  framebufferObj() = default;
  ~framebufferObj(){ if (FBO_colorbuffer) delete [] FBO_colorbuffer; }

  void init(unsigned int width, unsigned int height, Data data);

  void generateColorTexture(unsigned int width, unsigned int height, size_t i, Data data);
  void generateDepthTexture(unsigned int width, unsigned int height);

  //bind framebuffer to pipeline. We will  call this method in the render loop
  void bind(size_t i, unsigned int width, unsigned int height);

  //unbind framebuffer from pipeline. We will call this method in the render loop
  void unbind();

  void reset(unsigned int width, unsigned int height, Data data);

  GLenum getColorTexture(size_t i){ return FBO_colorbuffer[i];}

  void clear();

};
