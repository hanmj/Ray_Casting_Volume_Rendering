#pragma once

#include "loadRaw.h"
#include "glad/glad.h"
#include "GLFW/glfw3.h"

GLuint loadTexture(Data data);
